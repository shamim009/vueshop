import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  },
  {
    path: '/login',
    name: 'MasterLogin',
    component: () => import('../views/Admin/Auth/Master.vue'),
    redirect:{path:'login'},
    children:[
      {
        path: '/login',
        name: 'AdminLogin',
        component: () => import('../views/Admin/Auth/Login.vue'),
      }
    ]
  },
  {
    path: '/dashboard',
    name: 'DashboardMaster',
    component: () => import('../views/Admin/Master.vue'),
    redirect: {path: 'admin/dashboard'},
    children:[
      {
        path: '/admin/dashboard',
        name: 'Dashboard',
        component: () => import('../views/Admin/Dashboard.vue'),
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
