import Vue from 'vue'
import App from './App.vue'
import router from './router'
import bootstrapCss from '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Jquery from 'jquery'
window.$= window.jQuery = Jquery
import popper from 'popper.js'
import bootstrapJs from '../node_modules/bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap/dist/js/bootstrap.bundle';

import("../node_modules/admin-lte/dist/css/adminlte.min.css");
import("../node_modules/admin-lte/dist/js/adminlte.min.js");
import("../node_modules/admin-lte/plugins/fontawesome-free/css/all.css");

//var adminToken = window.localStorage.getItem('adminToken');

Vue.config.productionTip = false

new Vue({
  bootstrapCss,
  router,
  Jquery,
  popper,
  bootstrapJs,
  render: h => h(App)
}).$mount('#app')
